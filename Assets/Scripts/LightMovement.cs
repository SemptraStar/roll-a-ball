﻿using UnityEngine;
using System.Collections;

public class LightMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    public GameObject obj;
	// Update is called once per frame
	void Update () {       
        transform.position = obj.GetComponent<Transform>().position + Vector3.up;    
	}
}
