﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.position = player.transform.position + new Vector3(0, 6f, -5f);
    }

    public GameObject player;
    private float k;
    private Vector3 pointVector = new Vector3 (0f, 1f, -0.3f);
    // Update is called once per frame
    void LateUpdate () {
        k = 8f;      

        transform.position = player.transform.position + (pointVector * k);
    }
}