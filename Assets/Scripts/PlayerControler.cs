﻿using UnityEngine;
using System.Collections;

public class PlayerControler : MonoBehaviour
{

    Rigidbody playerRigid;

    // Use this for initialization
    void Start()
    {
        playerRigid = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private float jumpSpeed;
    //Physics
    [Range(0.1f, 10f)] public float speed = 1f;
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal") * speed;
        float moveVertical = Input.GetAxis("Vertical") * speed;

        jumpSpeed = 0f;

        if (Input.GetKeyDown(KeyCode.Space) && Physics.Raycast(transform.position, -Vector3.up * 0.5f, 0.7f))
            jumpSpeed = 180f;

        playerRigid.AddForce(moveHorizontal, jumpSpeed, moveVertical);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Collectable"))
            other.gameObject.SetActive(false);
    }
}