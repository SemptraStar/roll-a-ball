﻿using UnityEngine;
using System.Collections;

public class CollectableAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
        objTransform = GetComponent<Transform>();
        objTransform.rotation = Quaternion.Euler(45f, 45f, 45f);

        min = objTransform.position.y;
        max = min + 0.5f;
        sign = 1;
    }
	
    private Transform objTransform;
    [Range(0.5f, 5)] public float speed = 1f;
    private float min, max;
    private int sign;

	// Update is called once per frame
	void Update () {
        objTransform.rotation = Quaternion.Euler(Vector3.up + objTransform.rotation.eulerAngles);

        if (objTransform.position.y >= max) sign = -1;
        else if (objTransform.position.y <= min) sign = 1;

        float delta = Mathf.Abs((objTransform.position.y - (max + min) / 2));
        objTransform.position += new Vector3(0, (speed * 0.01f) - (delta * 0.005f), 0) * sign;
    }
}